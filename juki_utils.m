%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: juki_utils.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
% Small utilities.
%
%--------------------------------------------------%

:- module juki_utils.

%--------------------------------------------------%
:- interface.
:- import_module int, maybe, io.

%--------------------------------------------------%
% Nondet utilities

	% int_between(Low, High, Value):
	%
	% `Value` is an integer between `Low`..`High`.
	%
	% This is the same as `int.nondet_int_in_range`, but handles the
	% case where `Value` is already ground better.
	%
:- pred int_between(int, int, int).
:- mode int_between(in, in, out) is nondet.
:- mode int_between(in, in, in) is semidet.

%--------------------------------------------------%
% Higher order utilities

	% fold_while(P, !Acc):
	%
	% Call semidet predicate `P` repeatedly with `!Acc` until it
	% fails.
	%
:- pred fold_while(pred(T, T), T, T).
:- mode fold_while(pred(in, out) is semidet, in, out) is det.
:- mode fold_while(pred(mdi, muo) is semidet, mdi, muo) is det.
:- mode fold_while(pred(in, out) is cc_nondet, in, out) is cc_multi.
:- mode fold_while(pred(mdi, muo) is cc_nondet, mdi, muo) is cc_multi.

	% fold_while2(P, !Acc1, !Acc2):
	%
	% Call semidet predicate `P` repeatedly with `!Acc1` and `!Acc2`
	% until it fails.
	%
:- pred fold_while2(pred(T, T, U, U), T, T, U, U).
:- mode fold_while2(pred(in, out, in, out) is semidet, in, out, in, out) is det.
:- mode fold_while2(pred(in, out, mdi, muo) is semidet, in, out, mdi, muo) is det.
:- mode fold_while2(pred(mdi, muo, in, out) is semidet, mdi, muo, in, out) is det.
:- mode fold_while2(pred(mdi, muo, mdi, muo) is semidet, mdi, muo, mdi, muo) is det.
:- mode fold_while2(pred(in, out, in, out) is cc_nondet, in, out, in, out) is cc_multi.
:- mode fold_while2(pred(in, out, mdi, muo) is cc_nondet, in, out, mdi, muo) is cc_multi.
:- mode fold_while2(pred(mdi, muo, in, out) is cc_nondet, mdi, muo, in, out) is cc_multi.
:- mode fold_while2(pred(mdi, muo, mdi, muo) is cc_nondet, mdi, muo, mdi, muo) is cc_multi.

%--------------------------------------------------%
% Files

:- pred file_size(string, maybe(uint), io, io).
:- mode file_size(in, out, di, uo) is det.

%--------------------------------------------------%
% Math stuff

:- func gcd(int, int) = int.
:- mode gcd(in, in) = out is det.

%--------------------------------------------------%
:- implementation.

%--------------------------------------------------%
% Nondet utilities

:- pragma promise_equivalent_clauses(int_between/3).
int_between(Low::in, High::in, Value::out) :-
	Low =< High,
	(	Value = Low
	;	int_between(Low + 1, High, Value)
	).
int_between(Low::in, High::in, Value::in) :-
	Low =< Value,
	Value =< High.

%--------------------------------------------------%
% Higher order utilities

fold_while(P, !Acc1) :-
	(	call(P, !Acc1)
	->	fold_while(P, !Acc1)
	;	true
	).

fold_while2(P, !Acc1, !Acc2) :-
	(	call(P, !Acc1, !Acc2)
	->	fold_while2(P, !Acc1, !Acc2)
	;	true
	).

%--------------------------------------------------%
% Files

:- pragma foreign_decl("C", "#include <sys/stat.h>").

:- impure pred foreign_file_size(string::in, uint::out) is semidet.
:- pragma foreign_proc("C", foreign_file_size(Filename::in, Size::out),
	[will_not_call_mercury],
	"
	struct stat st;
	if (stat(Filename, &st)) {
		SUCCESS_INDICATOR = 0;
	} else {
		SUCCESS_INDICATOR = 1;
		Size = st.st_size;
	}
	"
).

:- pragma promise_pure(file_size/4).
file_size(Filename, Size, !IO) :-
	(	impure foreign_file_size(Filename, S0)
	->	Size = yes(S0)
	;	Size = no
	).

%--------------------------------------------------%
% Math stuff

gcd(A, B) = (B = 0 -> A ; gcd(B, A mod B)).

:- end_module juki_utils.
