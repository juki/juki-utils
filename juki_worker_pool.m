%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: juki_worker_pool.m
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
%
% This module implements a trivial worker pool.
%
%--------------------------------------------------%

:- module juki_worker_pool.

%--------------------------------------------------%
:- interface.
:- import_module io, maybe.
:- use_module thread, thread.future.

:- type thread_type
	--->	mercury
	;	native.

:- type worker_pool(T).

:- inst uniq_worker_pool == ground.

:- mode worker_pool_uo == out(uniq_worker_pool).
:- mode worker_pool_di == di(uniq_worker_pool).
:- mode worker_pool_ui == in(uniq_worker_pool).

:- pred init(
	maybe_error(worker_pool(T))::free >> maybe_error(uniq_worker_pool),
	thread_type::in,
	int::in,
	io::di, io::uo
) is cc_multi.

:- pred quit(worker_pool(T)::worker_pool_di, io::di, io::uo) is det.

:- pred run(worker_pool(T), pred(T), thread.future.future_io(T), io, io).
:- mode run(worker_pool_ui, (pred(out) is det), out, di, uo) is det.

:- pred run_io(worker_pool(T), pred(T, io, io), thread.future.future_io(T), io, io).
:- mode run_io(worker_pool_ui, (pred(out, di, uo) is det), out, di, uo) is det.

:- pred run_io_cc_multi(worker_pool(T), pred(T, io, io), thread.future.future_io(T), io, io).
:- mode run_io_cc_multi(worker_pool_ui, (pred(out, di, uo) is cc_multi), out, di, uo) is det.

%--------------------------------------------------%
:- implementation.
:- import_module int, list, exception.
:- use_module thread.channel.

:- type error
	--->	error(string).

:- type worker_pool(T)
	--->	worker_pool(
			threads :: list(thread.thread),
			task_channel :: thread.channel.channel(task(T))
		).

:- type task(T)
	--->	task(
			(pred(T::out) is det),
			thread.future.future_io(T)
		)
	;	io_task(
			(pred(T::out, io::di, io::uo) is det),
			thread.future.future_io(T)
		)
	;	io_task_cc_multi(
			(pred(T::out, io::di, io::uo) is cc_multi),
			thread.future.future_io(T)
		)
	;	quit.


init(MaybeWP, Type, N, !IO) :-
	thread.channel.init(Channel, !IO),
	(try [io(!IO)]
		thread_list(Threads, Type, N, Channel, !IO)
	then
		MaybeWP = ok(worker_pool(Threads, Channel))
	catch juki_worker_pool.error(ErrStr) ->
		% Make sure that any threads that might have been
		% successfully started shut down.
		thread.channel.put(Channel, quit, !IO),
		MaybeWP = error(ErrStr)
	).

run(worker_pool(_, Channel), TaskPred, Future, !IO) :-
	thread.future.init(Future, !IO),
	thread.channel.put(Channel, task(TaskPred, Future), !IO).

run_io(worker_pool(_, Channel), TaskPred, Future, !IO) :-
	thread.future.init(Future, !IO),
	thread.channel.put(Channel, io_task(TaskPred, Future), !IO).

run_io_cc_multi(worker_pool(_, Channel), TaskPred, Future, !IO) :-
	thread.future.init(Future, !IO),
	thread.channel.put(Channel, io_task_cc_multi(TaskPred, Future), !IO).

quit(worker_pool(_, Channel), !IO) :-
	thread.channel.put(Channel, quit, !IO).

:- pred thread_list(
	list(thread.thread)::out,
	thread_type::in,
	int::in,
	thread.channel.channel(task(T))::in,
	io::di, io::uo
) is cc_multi.
thread_list(ThreadList, Type, N, Channel, !IO) :-
	(	N = 0
	->	ThreadList = []
	;	thread_list(TL, Type, N - 1, Channel, !IO),
		(	Type = mercury,
			thread.spawn(worker(Channel), MaybeThread, !.IO, !:IO)
		;	Type = native,
			thread.spawn_native(worker(Channel), MaybeThread, !.IO, !:IO)
		),
		(	MaybeThread = ok(Thread)
		;	MaybeThread = error(Err),
			throw(juki_worker_pool.error(Err))
		),
		ThreadList = [Thread | TL]
	).

:- pred worker(thread.channel.channel(task(T)), thread.thread, io, io).
:- mode worker(in, in, di, uo) is cc_multi.
worker(Channel, Thread, !IO) :-
	thread.channel.take(Channel, Task, !IO),
	(	Task = task(P, Future),
		call(P, Value),
		thread.future.signal(Future, Value, !IO),
		worker(Channel, Thread, !IO)
	;	Task = io_task(P, Future),
		call(P, Value, !IO),
		thread.future.signal(Future, Value, !IO),
		worker(Channel, Thread, !IO)
	;	Task = io_task_cc_multi(P, Future),
		call(P, Value, !IO),
		thread.future.signal(Future, Value, !IO),
		worker(Channel, Thread, !IO)
	;	Task = quit,
		% Why doesn't `thread.channel.untake` work here?
		thread.channel.put(Channel, Task, !IO)
	).

:- end_module juki_worker_pool.
