%--------------------------------------------------%
%
% Copyright (c) 2019 Juha Kiiski
%
% Permission is hereby granted, free of charge, to any person
% obtaining a copy of this software and associated documentation files
% (the "Software"), to deal in the Software without restriction,
% including without limitation the rights to use, copy, modify, merge,
% publish, distribute, sublicense, and/or sell copies of the Software,
% and to permit persons to whom the Software is furnished to do so,
% subject to the following conditions:
%
% The above copyright notice and this permission notice shall be
% included in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
% NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
% BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
% ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
% CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
%
%--------------------------------------------------%
%
% File: juki_tester.m.
% Author: Juha Kiiski
% Email: juki_pub@outlook.com
% Dependencies: juki_worker_pool
%
% This module implements a simple testing framework.
%
% # Usage
%
% The module provides a predicate `test/3` to run test suites and to
% print out the results of the tests. A test suite is a collection of
% test cases defined with the constructor `test_suite/2`. For example,
%
%     :- func my_suite = test_suite.
%     my_suite = test_suite("My Suite", [
%         expect("Oddness 1", "11 is odd", odd(11)),
%         expect_not("Oddness 2", "10 is not odd", odd(10)),
%         expect("Oddness 3", "10 is odd", odd(10))
%     ]).
%
% This defines a suite with three tests for the standard library
% predicate `int.odd/1`. A report produced by
%
%     main(!IO) :-
%         test([my_suite], !IO).
%
% would look like
%
%     [date] - [grade]
%
%     SUITE: My Suite
%       FAILURE: Oddness 3                                  0.000s
%         10 is odd
%
%       SUMMARY (0.000s):
%         SUCCEEDED:     2
%         FAILED:        1
%         CANCELLED:     0
%         ERRORS:        0
%
% The exit status of the program will be 0 if all tests succeeded, 1
% if there were failures or 2 if there were errors. Cancelled tests do
% not affect exit status. The `test/3` predicate will look at the
% command line arguments for the program to determine which test
% suites to run and what kinds of results to display on the
% report. See supported arguments below.
%
% The `expect/3` and `expect_not/3` tests work for simple tests, but
% for more complex ones there are also `test/2` and `io_test/2`, which
% take a name and a det predicate that should have one output argument
% of type `test_result`. `io_test/2` cases also take in a pair of io
% state arguments. The predicate will indicate success or failure by
% producing either `success(Desc)`, `failure(Desc)`, `cancelled(Desc)`
% or `bad_state(Desc)` as the output.
%
% For cases where you need to use some kind of state across multiple
% tests, there are `init_state/2`, `test_state/2` and
% `destroy_state/2` cases. A state value should be initialized with a
% `init_state/2` test, and it will be threaded through the following
% `test_state/2` tests. If any of the tests fails, the state will be
% dropped and any subsequent tests using it will be automatically
% cancelled (it will still be passed to a `destroy_state/2` case
% though). If you need to initialize the state more than once inside
% the same suite, you must use a `destroy_state/2` test before the
% next `init_state/2` (otherwise the init case will just be cancelled,
% because there already is a state).
%
% The state is treated as a `univ` in order to allow different types
% of states in the same suite (but only one state can be live at
% once).
%
% # Command line arguments for `test/3`
%
% The suites to run can be filtered based on the name of the
% suite. The displayed results can by filtered by the type of the
% result. There are four kinds of results: successes, failures,
% cancelled (including for bad state) and errors (i.e. the test threw
% something rather than returning normally).
%
%     --suite <name> :: Only run suites whose name contains <name>
%     -t <name>      :: Only run suites whose name contains <name>
%     -a             :: Print all result with description
%     -A             :: Print all results without description
%     -s             :: Include successes with descriptions
%     -S             :: Include successes without descriptions
%     -f             :: Hide failures
%     -F             :: Hide failure descriptions
%     -c             :: Hide cancelled
%     -C             :: Hide cancel descriptions
%     -e             :: Hide errors
%     -E             :: Hide error descriptions
%     --jobs N       :: Run test suites in threads if supported
%     -jN            :: Run test suites in threads if supported
%
% # Predicates `run_tests/4` and `report_results/4`
%
% In case the `test/3` predicate doesn't suit your needs, there are
% also `run_tests/4` and `report_results/4` to run a test suite and to
% report results (from possibly multiple suites), respectively.
%
% The result structures are also exported, so that one may write their
% own reporting tools.
%
% # Bugs
%
% - [juki/2019-08-05] For some reason running with jobs on Win10 +
%   `asm_fast.par.gc.stseg` fails either with messy output (strings
%   pointing to bad addresses?) or no output at all. It works on
%   `hlc.par.gc`.
%
% # TODO
%
% - This module currently handles both running tests and reporting
%   results. Arguably it would be better to output the results in a
%   structured format (e.g. XML) and have a separate tool to display
%   the results as a human readable report. Maybe even have the tool
%   build the test program in all grades and compare the results.
%
% - Implement some kind of fuzzing tests with generated values.
%
%   - QUESTION [juki/2019-08-05]:
%
%     How would one define a test case that takes in a nondet
%     generator for values of any type, and a semidet predicate to
%     test the values? I managed to do this with a generator for a
%     specific type of values like
%
%         :- type test_pred
%            ---> %...
%            ; test_generator(name, pred(int), pred(int))
%
%         :- inst test_pred for test_pred/0
%            ---> %...
%            ; test_generator(ground, (pred(out) is nondet), (pred(in) is semidet))
%
%     but the type should really be something like
%
%         :- type test_pred
%            ---> %...
%            ; some [T] test_generator(name, pred(T), pred(T))
%
%     However this gives some cryptic error messages about the
%     predicate being `free` in the `do_while` inside the
%     corresponding clause of `run_test/6`.
%
% - Colorized output?.
%
%--------------------------------------------------%

:- module juki_tester.

%--------------------------------------------------%
:- interface.
:- import_module string, list, io, time, univ.

:- type name == string.
:- type desc == string.

:- type test_suite
	--->	test_suite(name, list(test_pred)).

	% Different kinds of test cases.
:- type test_pred
	--->	test(name, pred(test_result))
	;	io_test(name, pred(test_result, io, io))
	;	expect(name, desc, pred)
	;	expect_not(name, desc, pred)
	;	init_state(name, init_state_pred)
	;	test_state(name, test_state_pred)
	;	destroy_state(name, destroy_state_pred)
	% [juki/2019-08-05]: This is experimental and will likely change
	% in the future (see the TODO section above).
	;	test_generator(name, pred(int), pred(int)).

:- inst test_pred for test_pred/0
	--->	test(ground, pred(out) is det)
	;	io_test(ground, pred(out, di, uo) is det)
	;	expect(ground, ground, (pred) is semidet)
	;	expect_not(ground, ground, (pred) is semidet)
	;	init_state(ground, init_state_pred)
	;	test_state(ground, test_state_pred)
	;	destroy_state(ground, destroy_state_pred)
	;	test_generator(ground, (pred(out) is nondet), (pred(in) is semidet)).

:- type init_state_pred == pred(test_result, univ, io, io).
:- inst init_state_pred == (pred(out, out, di, uo) is det).
:- type test_state_pred == pred(test_result, univ, univ, io, io).
:- inst test_state_pred == (pred(out, in, out, di, uo) is det).
:- type destroy_state_pred == pred(test_result, univ, io, io).
:- inst destroy_state_pred == (pred(out, in, di, uo) is det).

	% A result returned by a test case. The result will be
	% automatically created for `expect/3` and `expect_not/3` tests,
	% while others kinds of test cases must return it.
:- type test_result
	--->	success(desc)
	;	failure(desc)
	;	cancelled(desc)
	;	bad_state(desc).

	% The results returned by `run_tests/4`. These can be used to write
	% custom reporting tools.
:- type suite_run_results
	--->	suite_run_results(name, list(run_result), summary).

:- type run_result
	--->	run_result(name, test_result, clock_t)
	;	error(name, desc, clock_t).

:- type summary
	--->	summary(
			int, % Number of successes
			int, % Number of failures
			int, % Number of cancelled tests
			int, % Number of errors
			clock_t % Total time taken for all tests
		).

	% test(TestSuites, !IO):
	%
	% Runs test suites and reports the results based on command line
	% arguments.
	%
	% This is the main entry point to the testing framework and
	% should usually be called from main, unless custom reporting or
	% test suite filtering is needed (those can be achieved by
	% calling `run_tests/4` directly).
	%
:- pred test(list(test_suite)::in, io::di, io::uo) is cc_multi.

:- pred run_tests(test_suite, suite_run_results, io, io).
:- mode run_tests(in, out, di, uo) is cc_multi.

:- type report_parameter
	--->	show_successes % Successes are hidden by default, while
	;	hide_failures  % others are visible.
	;	hide_cancelled % bad_state also counts as cancelled.
	;	hide_errors
	% The following determine whether the description should be
	% printed for different kinds of results. They only take effect
	% if the corresponding results are included.
	;	show_success_description
	;	hide_failure_description
	;	hide_cancel_description
	;	hide_error_description.

:- pred report_results(list(suite_run_results), list(report_parameter), io, io).
:- mode report_results(in, in, di, uo) is det.

%--------------------------------------------------%
:- implementation.
:- import_module int, float, char, bool, exception, getopt, calendar.
:- import_module thread, thread.future, maybe.
:- import_module solutions.
:- import_module juki_worker_pool.
:- use_module stream, stream.string_writer, string.builder.

%--------------------------------------------------%
% Running tests

	% Get the name of a test.
:- pred name(test_pred::in, name::out) is det.
name(test(Name, _), Name).
name(io_test(Name, _), Name).
name(expect(Name, _, _), Name).
name(expect_not(Name, _, _), Name).
name(init_state(Name, _), Name).
name(test_state(Name, _), Name).
name(destroy_state(Name, _), Name).
name(test_generator(Name, _, _), Name).

	% Count the successes, failures, cancels, errors and total time
	% for a list of results.
:- func summary(list(run_result)) = summary.
summary(Results) =
	foldl(
		(func(Result, summary(S, F, C, E, T)) = Sum :-
			(	Result = run_result(_, success(_), Time),
				Sum = summary(S + 1, F, C, E, T + Time)
			;	Result = run_result(_, failure(_), Time),
				Sum = summary(S, F + 1, C, E, T + Time)
			;	% Count bad state as cancelled.
				(	Result = run_result(_, cancelled(_), Time)
				;	Result = run_result(_, bad_state(_), Time)
				),
				Sum = summary(S, F, C + 1, E, T + Time)
			;	Result = error(_, _, Time),
				Sum = summary(S, F, C, E + 1, T + Time)
			)
		),
		Results,
		summary(0, 0, 0, 0, 0)
	).

:- type state_status
	--->	live(univ)
	;	dead(univ)
	;	none.

:- pred kill_state(state_status::in, state_status::out) is det.
kill_state(live(U), dead(U)).
kill_state(dead(U), dead(U)).
kill_state(none, none).

run_tests(test_suite(Name, Tests), SuiteResults, !IO) :-
	map_foldl2(run_test_wrapper, Tests, Results, none, _, !IO),
	SuiteResults = suite_run_results(Name, Results, summary(Results)).

	% Run a test case, catching any errors it might throw.
:- pred run_test_wrapper(test_pred, run_result, state_status, state_status, io, io).
:- mode run_test_wrapper(in(test_pred), out, in, out, di, uo) is cc_multi.
run_test_wrapper(Test, Result, !State, !IO) :-
	name(Test, Name),
	clock(Start, !IO),
	(try [io(!IO)]
		run_test(Test, TestResult, !State, !IO)
	then
		clock(End, !IO),
		Result = run_result(Name, TestResult, End - Start)
	catch_any AnyErr ->
		clock(End, !IO),
		kill_state(!State),
		% TODO: There is probably a better way to turn an
		% arbitrary object into a string, but I didn't see
		% anything in the library docs.
		SB0 = string.builder.init,
		stream.string_writer.print(string.builder.handle, AnyErr, SB0, SB1),
		Description = string.builder.to_string(SB1),
		Result = error(Name, Description, End - Start)
	).

:- pred run_test(test_pred, test_result, state_status, state_status, io, io).
:- mode run_test(in(test_pred), out, in, out, di, uo) is cc_multi.

run_test(test(_, P), Result, !State, !IO) :- call(P, Result).

run_test(io_test(_, P), Result, !State, !IO) :- call(P, Result, !IO).

run_test(expect(_, Description, P), Result, !State, !IO) :-
	Result = (call(P) -> success(Description) ; failure(Description)).

run_test(expect_not(_, Description, P), Result, !State, !IO) :-
	Result = (call(P) -> failure(Description) ; success(Description)).

run_test(init_state(_, P), Result, !State, !IO) :-
	(	!.State = live(_)
	->	Result = bad_state("There already is a state.")
	;	call(P, Result, NewState, !IO),
		!:State = (Result = success(_) -> live(NewState) ; none)
	).

run_test(test_state(_, P), Result, !State, !IO) :-
	(	!.State = live(StateIn),
		call(P, Result, StateIn, StateOut, !IO),
		!:State = (Result = success(_) -> live(StateOut) ; dead(StateIn))
	;	!.State = dead(_),
		Result = bad_state("Previous failure invalidated the state.")
	;	!.State = none,
		Result = bad_state("There is no state.")
	).

run_test(destroy_state(_, P), Result, !State, !IO) :-
	(	(	!.State = live(StateIn)
		;	!.State = dead(StateIn)
		)
	->	call(P, Result, StateIn, !IO),
		!:State = (Result = success(_) -> none ; !.State)
	;	Result = bad_state("There is no state.")
	).

run_test(test_generator(_, G, P), Result, !State, !IO) :-
	do_while(
		G,
		(pred(V::in, More::out, !.Acc::in, !:Acc::out) is det :-
			(	call(P, V)
			->	More = yes
			;	More = no,
				SB0 = string.builder.init,
				stream.string_writer.format(
					string.builder.handle,
					"Failed for value: ", [],
					SB0, SB1
				),
				stream.string_writer.print(
					string.builder.handle, V, SB1, SB2
				),
				Description = string.builder.to_string(SB2),
				!:Acc = failure(Description)
			)
		),
		success("All values succeeded."),
		Result
	).

%--------------------------------------------------%
% Reporting

report_results(AllResults, Params, !IO) :-
	current_utc_time(Date, !IO),
	format("UTC %s - %s\n", [s(date_to_string(Date)), s($grade)], !IO),
	foldl2(
		(pred(suite_run_results(Name, Results, Summary)::in, !.Sum::di, !:Sum::uo, !.IO::di, !:IO::uo) is det :-
			format("\nSUITE: %s\n", [s(Name)], !IO),
			foldl(report_result(Params), Results, !IO),
			Summary = summary(S, F, C, E, Time),
			format(
				"\n  SUMMARY: %.3fs\n"
				++ "    SUCCEEDED: %5d\n"
				++ "    FAILED:    %5d\n"
				++ "    CANCELLED: %5d\n"
				++ "    ERRORS:    %5d\n",
				[
					f(float(Time) / float(clocks_per_sec)),
					i(S), i(F), i(C), i(E)
				],
				!IO
			),
			!.Sum = summary(RS, RF, RC, RE, RTime),
			!:Sum = summary(RS + S, RF + F, RC + C, RE + E, RTime + Time)
		),
		AllResults,
		% Full summary of all test suites.
		summary(0, 0, 0, 0, 0),
		summary(FS, FF, FC, FE, FTime),
		!IO
	),
	(	length(AllResults) > 1
	->	format(
			"\nFULL SUMMARY: %.3fs\n"
			++ "  SUCCEEDED: %5d\n"
			++ "  FAILED:    %5d\n"
			++ "  CANCELLED: %5d\n"
			++ "  ERRORS:    %5d\n",
			[
				f(float(FTime) / float(clocks_per_sec)),
				i(FS), i(FF), i(FC), i(FE)
			],
			!IO
		)
	;	true
	).

:- pred report_result(list(report_parameter), run_result, io, io).
:- mode report_result(in, in, di, uo) is det.

report_result(Params, run_result(Name, success(Description), Time), !IO) :-
	(	member(show_successes, Params)
	->	format(
			"  SUCCESS: %-50s %10.3fs\n",
			[
				s(Name),
				f(float(Time) / float(clocks_per_sec))
			],
			!IO
		),
		(	member(show_success_description, Params)
		->	format("    %s\n", [s(Description)], !IO)
		;	true
		)
	;	true
	).

report_result(Params, run_result(Name, failure(Description), Time), !IO) :-
	(	\+ member(hide_failures, Params)
	->	format(
			"  FAILURE: %-50s %10.3fs\n",
			[
				s(Name),
				f(float(Time) / float(clocks_per_sec))
			],
			!IO
		),
		(	\+ member(hide_failure_description, Params)
		->	format("    %s\n", [s(Description)], !IO)
		;	true
		)
	;	true
	).

report_result(Params, run_result(Name, cancelled(Description), Time), !IO) :-
	(	\+ member(hide_cancelled, Params)
	->	format(
			"  CANCEL: %-51s %10.3fs\n",
			[
				s(Name),
				f(float(Time) / float(clocks_per_sec))
			],
			!IO
		),
		(	\+ member(hide_cancel_description, Params)
		->	format("    %s\n", [s(Description)], !IO)
		;	true)
	;	true
	).

report_result(Params, run_result(Name, bad_state(Description), Time), !IO) :-
	(	\+ member(hide_cancelled, Params)
	->	format(
			"  BAD_STATE: %-48s %10.3fs\n",
			[
				s(Name),
				f(float(Time) / float(clocks_per_sec))
			],
			!IO
		),
		(	\+ member(hide_cancel_description, Params)
		->	format("    %s\n", [s(Description)], !IO)
		;	true)
	;	true
	).

report_result(Params, error(Name, Description, Time), !IO) :-
	(	\+ member(hide_errors, Params)
	->	format(
			"  ERROR: %-52s %10.3fs\n",
			[
				s(Name),
				f(float(Time) / float(clocks_per_sec))
			],
			!IO
		),
		(	\+ member(hide_error_description, Params)
		->	format("    %s\n", [s(Description)], !IO)
		;	true)
	;	true
	).

%--------------------------------------------------%
% `test/3`

:- type cmdline_option
	--->	suite
	;	report_all
	;	report_all_with_description
	;	report_success
	;	report_success_description
	;	dont_report_failure
	;	dont_report_failure_description
	;	dont_report_cancelled
	;	dont_report_cancelled_description
	;	dont_report_error
	;	dont_report_error_description
	;	jobs.

:- pred short_option(char::in, cmdline_option::out) is semidet.
short_option('t', suite).
short_option('a', report_all_with_description).
short_option('A', report_all).
short_option('s', report_success_description).
short_option('S', report_success).
short_option('f', dont_report_failure).
short_option('F', dont_report_failure_description).
short_option('c', dont_report_cancelled).
short_option('C', dont_report_cancelled_description).
short_option('e', dont_report_error).
short_option('E', dont_report_error_description).
short_option('j', jobs).

:- pred long_option(string::in, cmdline_option::out) is semidet.
long_option("suite", suite).
long_option("jobs", jobs).

:- pragma no_determinism_warning(option_default/2).
:- pred option_default(cmdline_option::out, option_data::out) is nondet.
option_default(suite, accumulating([])).
option_default(report_all, bool(no)).
option_default(report_all_with_description, bool(no)).
option_default(report_success, bool(no)).
option_default(report_success_description, bool(no)).
option_default(dont_report_failure, bool(no)).
option_default(dont_report_failure_description, bool(no)).
option_default(dont_report_cancelled, bool(no)).
option_default(dont_report_cancelled_description, bool(no)).
option_default(dont_report_error, bool(no)).
option_default(dont_report_error_description, bool(no)).
option_default(jobs, int(1)).

:- pred suite_filter_from_cmdline(option_table(cmdline_option), pred(test_suite)).
:- mode suite_filter_from_cmdline(in, out(pred(in) is semidet)) is det.
suite_filter_from_cmdline(Opts, Filter) :-
	lookup_accumulating_option(Opts, suite, Suites),
	(	Suites \= []
	->	Filter = (pred(test_suite(Name, _)::in) is semidet :-
			any_true(
				(pred(Suite::in) is semidet :-
					% REVISIT IN FUTURE [juki/2019-08-05]:
					% The standard library doesn't seem to
					% have a case folding sub-string search
					% predicate. In fact, there doesn't seem
					% to be unicode case folding available
					% at all.
					sub_string_search(to_lower(Name), to_lower(Suite), _)
				),
				Suites
			)
		)
	;	Filter = (pred(_::in) is semidet)
	).

:- pred report_params_from_cmdline(option_table(cmdline_option), list(report_parameter)).
:- mode report_params_from_cmdline(in, out) is det.
report_params_from_cmdline(Opts, Params) :-
	(	lookup_bool_option(Opts, report_all_with_description, yes)
	->	Params = [show_successes, show_success_description]
	;	lookup_bool_option(Opts, report_all, yes)
	->	Params = [
			show_successes,
			hide_failure_description,
			hide_cancel_description,
			hide_error_description
		]
	;	(	lookup_bool_option(Opts, report_success_description, yes)
		->	S = [show_successes, show_success_description]
		;	lookup_bool_option(Opts, report_success) = yes
		->	S = [show_successes]
		;	S = []
		),
		(	lookup_bool_option(Opts, dont_report_failure_description, yes)
		->	F = [hide_failure_description]
		;	lookup_bool_option(Opts, dont_report_failure) = yes
		->	F = [hide_failures]
		;	F = []
		),
		(	lookup_bool_option(Opts, dont_report_cancelled_description, yes)
		->	C = [hide_cancel_description]
		;	lookup_bool_option(Opts, dont_report_cancelled) = yes
		->	C = [hide_cancelled]
		;	C = []
		),
		(	lookup_bool_option(Opts, dont_report_error_description, yes)
		->	E = [hide_error_description]
		;	lookup_bool_option(Opts, dont_report_error, yes)
		->	E = [hide_errors]
		;	E = []
		),
		Params = S ++ F ++ C ++ E
	).

:- pred run_suites_without_threads(list(test_suite), list(suite_run_results), io, io).
:- mode run_suites_without_threads(in, out, di, uo) is cc_multi.
run_suites_without_threads(Suites, Results, !IO) :-
	map_foldl(
		(pred(Suite::in, Res::out, !.IO::di, !:IO::uo) is cc_multi :-
			run_tests(Suite, Res, !IO)
		),
		Suites, Results, !IO
	).

:- pred run_suites_with_threads(int, list(test_suite), list(suite_run_results), io, io).
:- mode run_suites_with_threads(in, in, out, di, uo) is cc_multi.
run_suites_with_threads(Jobs, Suites, Results, !IO) :-
	juki_worker_pool.init(MaybeWP, juki_worker_pool.mercury, Jobs, !IO),
	(	MaybeWP = ok(WP),
		map_foldl(
			(pred(Suite::in, F::out, !.IO::di, !:IO::uo) is cc_multi :-
				juki_worker_pool.run_io_cc_multi(
					WP,
					(pred(Res::out, !.IO::di, !:IO::uo) is cc_multi :-
						run_tests(Suite, Res, !IO)
					),
					F, !IO
				)
			),
			Suites, ResultFutures,
			!IO
		),
		map_foldl(wait, ResultFutures, Results, !IO),
		juki_worker_pool.quit(WP, !IO)
	;	MaybeWP = error(_),
		run_suites_without_threads(Suites, Results, !IO)
	).

test(Suites, !IO) :-
	command_line_arguments(Args, !IO),
	process_options(
		option_ops(short_option, long_option, option_default),
		Args, _, _, Result
	),
	(	Result = ok(Opts),
		suite_filter_from_cmdline(Opts, Filter),
		FilteredSuites = filter(Filter, Suites),
		report_params_from_cmdline(Opts, Params),
		lookup_int_option(Opts, jobs, Jobs)
	;	Result = error(_Err),
		FilteredSuites = Suites,
		Params = [],
		Jobs = 1
	),
	(	FilteredSuites \= []
	->	(	Jobs > 1
		->	run_suites_with_threads(Jobs, Suites, Results, !IO)
		;	run_suites_without_threads(Suites, Results, !IO)
		),
		report_results(Results, Params, !IO),
		% Set exit status to an error code if there were failures
		% or errors. Cancelled tests do not cause an error code to
		% be returned.
		(	% Exit status 2 if there were errors.
			any_true(
				(pred(suite_run_results(_, _, summary(_, _, _, E, _))::in) is semidet :-
					E > 0
				),
				Results
			)
		->	set_exit_status(2, !IO)
		;	% Exit status 1 if there were failures.
			any_true(
				(pred(suite_run_results(_, _, summary(_, F, _, _, _))::in) is semidet :-
					F > 0
				),
				Results
			)
		->	set_exit_status(1, !IO)
		;	true
		)
	;	format("No test suites to run.\n", [], !IO)
	).

:- end_module juki_tester.
